extern crate clap;
extern crate rusoto_core;
extern crate rusoto_sts;
extern crate rusoto_cloudformation;

use clap::{App, Arg, SubCommand};

mod stack;

use std::str::FromStr;
use std::{env, process};

use rusoto_cloudformation::{CloudFormation, CloudFormationClient};
use rusoto_core::{Region};
use rusoto_sts::{StsClient, StsAssumeRoleSessionCredentialsProvider};
use rusoto_core::reactor::RequestDispatcher;

pub fn main() {
    let mut app = App::new("cfnutil")
                    .version("0.1a")
                    .author("Jonathan Johnson <jon@nilobject.com>")
                    .arg(Arg::with_name("region")
                        .short("r")
                        .long("region")
                        .value_name("REGION")
                        .help("The region to execute the CloudFormation stack in")
                        .takes_value(true)
                    )
                    .arg(Arg::with_name("role")
                        .long("role")
                        .value_name("ROLE")
                        .help("Assumes this role before interacting with CloudFormation")
                        .takes_value(true)
                    )
                    .subcommand(SubCommand::with_name("stack")
                        .about("operations to do with a single CloudFormation stack")
                        .arg(Arg::with_name("STACKNAME")
                            .help("The stack name to use")
                            .required(true)
                        )
                        .subcommand(SubCommand::with_name("wait")
                            .help("Monitor the stack until it reaches a certain state")
                            .subcommand(SubCommand::with_name("ready")
                                .help("Monitor the stack until it reaches a CREATE_COMPLETE, UPDATE_COMPLETE, or ROLLBACK_COMPLETE")
                            )
                        )
                    );
    
    if env::args().len() == 1 {
        let _ = app.print_help();
        process::exit(-1);
    }

    let matches = app.get_matches();

    let region = get_region(matches.value_of("region"));
    let client = assume_role_if_needed(&region, matches.value_of("role"));

    if let Some(stack_matches) = matches.subcommand_matches("stack") {
        execute_stack_command(stack_matches, client.as_ref());
    }
}

fn assume_role_if_needed(region: &Region, role: Option<&str>) -> Box<CloudFormation> {
    return match role {
        Some(role) => {
            let sts = StsClient::simple(region.to_owned());
            let provider = StsAssumeRoleSessionCredentialsProvider::new(
                sts,
                role.to_owned(),
                "cfnutil".to_owned(),
                None, None, None, None
            );
            Box::new(CloudFormationClient::new(RequestDispatcher::default(), provider, region.to_owned())) as Box<CloudFormation>
        },
        None => Box::new(CloudFormationClient::simple(region.to_owned())) as Box<CloudFormation>,
    };
}

fn get_region(region: Option<&str>) -> Region {
    return match region {
        Some(region_name) => {
            match Region::from_str(region_name) {
                Ok(region) => region,
                Err(message) => {
                    println!("Unknown region: {:?}", message);
                    process::exit(-1);
                }
            }
        },
        None => Region::default(),
    };
}

fn execute_stack_command(stack_matches: &clap::ArgMatches, client: &CloudFormation) {
    let stack_name = stack_matches.value_of("STACKNAME").unwrap();

    if let Some(wait_matches) = stack_matches.subcommand_matches("wait") {
        execute_stack_wait_command(stack_name, wait_matches, client)
    }
}

fn execute_stack_wait_command(stack_name: &str, wait_matches: &clap::ArgMatches, client: &CloudFormation)
{
    if let Some(_ready_matches) = wait_matches.subcommand_matches("ready") {
        stack::wait::ready(client, stack_name);
    }
}